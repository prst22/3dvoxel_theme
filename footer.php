

		<footer>
			<div class="container grid-xl">
			  <div class="columns">
			    <div class="column col-sm-12 col-md-4 col-lg-3 col-xl-2 col-2">
			    	<div class="footer_logo h">
						<a href="<?php echo home_url();?>" title="Go home" class="main_site_logo">
							<img src="<?php echo esc_url(get_template_directory_uri() . '/assets/logo.png'); ?>" alt="<?php bloginfo('name'); ?>">
						</a>
						<?php 

							$args = array( 
									'post_type'		=> 'games',
			        				'post_status'	=> 'publish',
			        				'orderby'		=> 'rand',
			        				'order'			=> 'ASC',
									'posts_per_page' => 1,
									'no_found_rows' => true, 
									'update_post_meta_cache' => false, 
									'update_post_term_cache' => false, 
									'fields' => 'ids'
								 );
							$loop = new WP_Query( $args );
							if ($loop->have_posts()) : while ($loop->have_posts()) : $loop->the_post();
						?>
							<a href="<?php the_permalink(); ?>" class="btn btn-primary yellow_btn rand_btn" title="Random game"><span class="pr-1"><?php echo __( 'random', '3dvoxel-theme' ); ?></span><span class="icon icon-dice-with-five-dots"></span></a>
						<?php	
							wp_reset_postdata();
								endwhile;
						endif;

						 ?>
						
					</div>
			    </div>
			    <div class="column col-sm-12 col-md-4 col-lg-6 col-xl-8 col-8">
			    	<div class="bottom_copy">
						<span class="icon icon-copyright pr-1"></span>
						<?php echo date("Y") ;?>&nbsp;-&nbsp;<a href="<?php echo home_url();?>"><?php echo bloginfo('name'); ?></a>
					</div> 
			    </div>
			    <div class="column col-sm-12 col-md-4 col-lg-3 col-xl-2 col-2">
			    	<div class="scroll_top_cnt h">
			    		<button class="btn btn-primary btn-action" id="jump"><span class="icon icon-arrow-up-thick"></span></button>
			    	</div>
			    </div>
			  </div>
			</div>
			
		</footer>
	</div><!-- site_wrap -->
</div><!--menu wrapper for blur effect -->

<!-- mobile menu start -->
<?php if ( has_nav_menu( 'main_menu' ) ) : ?>
	<section class="mobile_menu_wrapper">

		<nav class="mobile_menu_cnt hidden">
			<div class="mobile_menu_cnt__inner hidden">
				<div class="menu_content">
					<h1>MENU</h1>
	                <div class="close_menu_cnt">
	                	<button class="btn btn-primary btn-action" id="close_menu_btn">
	                		<span class="icon icon-close"></span>
	                	</button>
						
	                </div>
					<?php
					   wp_nav_menu( array(
							   'menu'              => 'Main menu',
							   'theme_location'    => 'main_menu',
							   'container_class'   => 'mobile_menu_list_cnt',
							   'menu_class'        => 'mobile_menu_list_cnt__mobile_list',
							   'depth'             => 1,               
						   )
					   );
					?>
				</div>
			</div>
		</nav>

	</section> 
<?php endif; ?>
<!-- mobile menu end -->
</div><!--menu wrapper for blur effect search -->
<?php wp_footer(); ?>

</body>
</html>