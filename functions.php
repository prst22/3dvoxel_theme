<?php

require_once get_stylesheet_directory() . '/inc/custom-metaboxes.php';
require_once get_stylesheet_directory() . '/inc/ajax.php';
require_once get_stylesheet_directory() . '/inc/admin.php';

function register_voxel_session(){
  if( !session_id() ){
    session_start();
  }
}
add_action('init', 'register_voxel_session', 1);

function theme_setup() {
	load_theme_textdomain( '3dvoxel-theme' );

	// Adding menus to wp controll pannel
	register_nav_menus(array(
		'main_menu' =>__('Main menu')
	));

	//post thum pictures
    add_theme_support('post-thumbnails');

	add_image_size('small-thumbnail', 180, 101, array('center','center'));
    add_image_size('medium-thumbnail', 577, 325, array('center','center'));  
    add_image_size('large-thumnbail', 1200, 675, array('center','center'));

    add_theme_support( 'post-formats', array( 'video' ));

    add_theme_support( 'customize-selective-refresh-widgets' );

}
add_action( 'after_setup_theme', 'theme_setup' );

//add theme support end


//adding css styles start
function add_styles_js(){
	wp_enqueue_style('style', get_theme_file_uri( '/css/main.min.css'),  array(), '3.9.5');
	wp_enqueue_script('scroll_rev', get_template_directory_uri() . '/js/scrollreviel.min.js', array(), 1.0, false);
	wp_enqueue_script('main_js', get_template_directory_uri() . '/js/main.min.js', array('jquery'), 3.1, true);

    if(is_singular('games')){
       wp_localize_script('main_js', 'postID', array( 'id' => get_the_ID() ));  
       wp_enqueue_script('adblock_checker', get_stylesheet_directory_uri() . '/js/ads.js', array(), 1.0, false); 
    }

	if (is_singular() && comments_open() && get_option( 'thread_comments' )) {
		wp_enqueue_script( 'comment-reply' );
	}

    if(is_page_template('page-suggest.php')){
        wp_enqueue_script('camp_submit', get_stylesheet_directory_uri() . '/js/game-submit.js', array(), 1.5, true);
    }

    if(is_page_template('page-favourite.php')){
        wp_enqueue_script('favourite', get_stylesheet_directory_uri() . '/js/favourite.min.js', array(), 1, true);
        wp_localize_script('favourite', 'url', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ));
    }
}
add_action('wp_enqueue_scripts', 'add_styles_js');
//adding css styles end

function set_excerpt_length(){
    if(has_post_thumbnail()){
        return 15;  
    }else{
        return 30;
    }  	
}
add_filter('excerpt_length', 'set_excerpt_length');

//cpt start
function custom_post_type() {
// Set UI labels for Custom Post Type
    $labels = array(
        'name'                => _x( 'Game', 'Post Type General Name', '3dvoxel-theme' ),
        'singular_name'       => _x( 'Game', 'Post Type Singular Name', '3dvoxel-theme' ),
        'menu_name'           => __( 'Games', '3dvoxel-theme' ),
        'parent_item_colon'   => __( 'Parent Games', '3dvoxel-theme' ),
        'all_items'           => __( 'All Games', '3dvoxel-theme' ),
        'view_item'           => __( 'View Games', '3dvoxel-theme' ),
        'add_new_item'        => __( 'Add New Games', '3dvoxel-theme' ),
        'add_new'             => __( 'Add New', '3dvoxel-theme' ),
        'edit_item'           => __( 'Edit Game', '3dvoxel-theme' ),
        'update_item'         => __( 'Update Games', '3dvoxel-theme' ),
        'search_items'        => __( 'Search Games', '3dvoxel-theme' ),
        'not_found'           => __( 'Not Found', '3dvoxel-theme' ),
        'not_found_in_trash'  => __( 'Not found in Trash', '3dvoxel-theme' ),
    );
     
// Set other options for Custom Post Type	     
    $args = array(
        'label'               => __( 'games', '3dvoxel-theme' ),
        'description'         => __( 'Games', '3dvoxel-theme' ),
        'labels'              => $labels,
        // Features this CPT supports in Post Editor
        'supports'            => array( 'title', 'editor', 'thumbnail', 'revisions' ),
        /* A hierarchical CPT is like Pages and can have
        * Parent and child items. A non-hierarchical CPT
        * is like Posts.
        */ 
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 10,
        'can_export'          => true,  
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
        'menu_icon'           => 'dashicons-images-alt',
        'taxonomies'          => array( 'game_types' ),
        'has_archive'         => true,
    );

    // Registering your Custom Post Type
    register_post_type( 'games', $args );
}
 
/* Hook into the 'init' action so that the function
* Containing our post type registration is not 
* unnecessarily executed. 
*/
add_action( 'init', 'custom_post_type', 0 );

// create two taxonomies, genres and writers for the post type "book"
function game_types_tax() {
    // Add new taxonomy, make it hierarchical (like categories)
    $labels = array(
        'name'              => _x( 'Game types', 'taxonomy general name', '3dvoxel-theme' ),
        'singular_name'     => _x( 'Game type', 'taxonomy singular name', '3dvoxel-theme' ),
        'search_items'      => __( 'Search For Game type', '3dvoxel-theme' ),
        'all_items'         => __( 'All Game types', '3dvoxel-theme' ),
        'parent_item'       => __( 'Parent type', '3dvoxel-theme' ),
        'parent_item_colon' => __( 'Parent type:', '3dvoxel-theme' ),
        'edit_item'         => __( 'Edit type', '3dvoxel-theme' ),
        'update_item'       => __( 'Update type', '3dvoxel-theme' ),
        'add_new_item'      => __( 'Add New type', '3dvoxel-theme' ),
        'new_item_name'     => __( 'New type Name', '3dvoxel-theme' ),
        'menu_name'         => __( 'Game types', '3dvoxel-theme' ),
    );

    $args = array(
        'hierarchical'      => true,
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
    );

    register_taxonomy( 'game_types', 'games', $args );
}
add_action( 'init', 'game_types_tax', 0 );
// register custom taxonomy for clients post type end 

//cpt end

function custom_excerpt() {
    return '... ';
}
add_filter( 'excerpt_more', 'custom_excerpt' );

// adding class names to previous and next pagination links of get_next/previous_posts_link start

add_filter('next_posts_link_attributes', 'posts_link_attributes');
add_filter('previous_posts_link_attributes', 'posts_link_attributes');

function posts_link_attributes() {
    return (current_filter() == 'next_posts_link_attributes') ? 'class="btn btn-primary next_post_link"' : 'class="btn btn-primary previous_post_link"';
}
// adding class names to previous and next pagination links of get_next/previous_posts_link end

// add hew bage to games start
function voxel_new_badge($post_time_stamp, $cur_time, $post_id){
    $php_date = date("Y/m/d", $post_time_stamp);
    $delta_time = $cur_time - $post_time_stamp;
    $bage_text = __( 'NEW!', '3dvoxel-theme' );
    $bage = '<span class="new_bage">' . $bage_text . '</span>';
    $output = $delta_time <= 604800 ? $bage : '';
    return $output;
}
// add hew bage to games end

// count amount of views of a game cpt start
function save_game_views( $postID ) {
        
    $metaKey = 'voxel_theme_game_views';
    $views = get_post_meta( $postID, $metaKey, true );
    
    $count = ( empty( $views ) ? 0 : $views );
    $count++;
    
    update_post_meta( $postID, $metaKey, $count );
    
}
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
// count amount of views of a game cpt end

//exclude from search

function my_post_queries( $query ) {
    // do not alter the query on wp-admin pages and only alter it if it's the main query

    if (!is_admin() && $query->is_main_query()){

        if (is_search()) {
            $query->set('post_type', array('games'));
        }
    }
}
add_action( 'pre_get_posts', 'my_post_queries' );
