<?php get_header(); ?>
    <main class="container grid-xl single_post_main">
        <div class="columns">
            <div class="column col-12">
                <?php
                // Start the loop.

                while ( have_posts() ) : the_post(); ?>

                    <div class="single_post_main__inner">

                    <?php
                        get_template_part( 'template-parts/content');
                    ?>
                    </div>
                    <?php
                    // If comments are open or we have at least one comment, load up the comment template.
                    ?>
                    <?php
                    
                // End the loop.
                endwhile;
                ?>
            </div>
        </div>
    </main><!-- .site-main -->
<?php get_footer(); ?>