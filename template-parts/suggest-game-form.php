<div class="columns">
	<div class="column col-xl-12 col-12">
		<p>Two rules for your game to be published:</p>
		<ul>
			<li>Your game must be well made and playable. No prototypes!</li>
			<li>Your 3D world must be appealing for many players</li>
		</ul>

		<p>This site is made since there are now so many users making games with Unity and Adventure Box 3D engine. I will try to publish the new games I can find. Please tell me if you find any more good games that I can publish online.</p>

		<p><i>All fields are required</i></p>
	</div>
</div> 	
<div class="columns">
    <div class="column col-md-12 col-8">
    	<h2>Game submit:</h2>
		<form id="game_submit" class="" action="#" method="post" data-url="<?php echo admin_url('admin-ajax.php'); ?>">

			<div class="input-group py-2">
			  <span class="input-group-addon addon-lg">GAME TITLE</span>
			  <input type="text" class="form-input input-lg" placeholder="..." id="name" name="name">
			</div>

			<div class="input-group py-2">
			  <span class="input-group-addon addon-lg">YOUR EMAIl</span>
			  <input type="email" class="form-input input-lg" placeholder="..." id="email" name="email">
			  
			</div>

			<div class="input-group py-2">
			  <span class="input-group-addon addon-lg">GAME LINK</span>
			  <input type="url" class="form-input input-lg" placeholder="http://example.com/" id="game_link" name="game_link">
			</div>
			<div class="form-group py-2">
				<div>Game type:</div>
				<div><small><i>Choose one</i></small></div>
			  	<label class="form-checkbox form-inline">
			    	<input type="checkbox" id="battlefield" name="game_type_battlefield"><i class="form-icon"></i> Battlefield
			  	</label>
			  	<label class="form-checkbox form-inline">
			    	<input type="checkbox" id="open_world" name="game_type_open_world"><i class="form-icon"></i> Open world
			  	</label>
			  	<label class="form-checkbox form-inline">
			    	<input type="checkbox" id="other" name="game_type_other"><i class="form-icon"></i> Other
			  	</label>
			</div>
			<div class="form-group py-2">
				<label class="form-label" for="game_description">GAME DESCRIPTION</label>
				<textarea class="form-input" id="game_description" placeholder="GAME DESCRIPTION" rows="6" name="game_description"></textarea>
			</div>
			
			<div class="form-group py-2 p-relative">
				<button type="submit" class="btn btn-primary">Submit Game</button>
				<i class="form-icon loading hidden_form_inf" id="loader"></i>
				
			</div>
			<div class="py-1 text-center">
				<small class="game_success hidden_form_inf"><b>GAME SUBMITTED, THANKS!</b></small>
			</div>
			<div class="py-1 text-center">
				<small class="game_error hidden_form_inf"><b>SOMETHING WENT WRONG, TRY AGAIN LATER</b></small>
			</div>

		</form>
	</div>
	<div class="column col-md-12 hide-md col-4 form_image">
		<img src="<?php echo esc_url(get_template_directory_uri() . '/assets/submit.svg'); ?>" alt="share game image">
	</div>
</div>
