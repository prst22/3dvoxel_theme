<?php

	$term = get_term( get_the_ID() );
	$cur_time = time();
        
 ?>
<article <?php post_class( array('index_game', 'column', 'col-sm-12', 'col-md-6', 'col-lg-6', 'col-xl-4', 'col-3') ); ?>>
	<div class="index_game__inner">
		<?php 
			$post_date = get_the_date( 'U', get_the_ID() );
			echo voxel_new_badge($post_date, $cur_time, get_the_ID());
			$views = get_post_meta(get_the_ID(), 'voxel_theme_game_views', TRUE);
		?>
		<div class="index_game__inner--bg">

			<?php if(has_post_thumbnail()): ?><!-- has thumb start -->
				<figure class="thumbnail_cnt">
					<span class="thumbnail_cnt__game_views" title="game views">
						<span class="icon icon-eye"></span>
						<?php 
							
							$print_views = empty($views) ? 0 : $views;
							echo "<span>{$print_views}</span>";
						?>
					</span>
					<div class="thumbnail_cnt__inner">
						<a href="<?php the_permalink(); ?>" title="Link to <?php the_title_attribute(); ?>" class="thumbnail_cnt__link">
							<?php 
								the_post_thumbnail('medium-thumbnail', 
								$attr = array('class' => "thumbnail",
										'alt' => esc_attr(get_the_title()),
									)); ?>
	                    </a> 
					</div>

					<?php if(get_post_type() === 'games'): ?>

					<div class="pad_outer">
						<?php
							$game_types = get_the_terms( get_the_ID(), 'game_types' );

							if(!empty($game_types)):

								foreach( $game_types as $type ) {
									
									if($type->slug == 'battlefield'){
										$game_type_icon = sprintf(

									        '<div class="pad pad--type">
									        	<a href="%1$s" title="%2$s">
									        		<span class="icon icon-revolver"></span>
									        	</a>
									        	</div>',
									        esc_url( get_category_link( $type->term_id ) ),
									        esc_attr( sprintf( __( 'View all games in %s category', '3dvoxel-theme' ), $type->name ) ),
									        esc_html__($type->count),
									        esc_html( $type->name )
									    );
			 
										echo $game_type_icon; 
									}

									if($type->slug == 'open-world'){
										$game_type_icon = sprintf(

									        '<div class="pad pad--type">
									        	<a href="%1$s" title="%2$s">
									        		<span class="icon icon-earth"></span>
									        	</a>
									        	</div>',
									        esc_url( get_category_link( $type->term_id ) ),
									        esc_attr( sprintf( __( 'View all games in %s category', '3dvoxel-theme' ), $type->name ) ),
									        esc_html__($type->count),
									        esc_html( $type->name )
									    );
			 
										echo $game_type_icon; 
									}

									if($type->slug == 'other'){
										$game_type_icon = sprintf(

									        '<div class="pad pad--type">
									        	<a href="%1$s" title="%2$s">
									        		<span class="icon icon-dots-three-horizontal"></span>
									        	</a>
									        	</div>',
									        esc_url( get_category_link( $type->term_id ) ),
									        esc_attr( sprintf( __( 'View all games in %s category', '3dvoxel-theme' ), $type->name ) ),
									        esc_html__($type->count),
									        esc_html( $type->name )
									    );
			 
										echo $game_type_icon; 
									}
								     
								} 
							endif;
						?>

						<div class="pad">
							<span class="icon icon-gamepad"></span>
						</div>
						
					</div>
					<?php endif; ?>

				</figure> 			

			<?php endif;?><!-- has thumb end -->
			<h3 class="index_game__heading">
				<a href="<?php the_permalink() ?>" title="Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a>
			</h3>
			
			<div class="index_game__excerpt">
				<?php the_excerpt(); ?>
			</div>
			<?php if(get_post_type() === 'games'): ?>
				<div class="index_game__play">
					<a class="btn btn-primary btn_play" href="<?php the_permalink(); ?>">PLAY</a>
				</div>
				<?php else: ?>
				<div class="index_game__play">
					<a class="btn btn-primary btn_play" href="<?php the_permalink(); ?>">READ MORE</a>
				</div>
			<?php endif; ?>

		</div>
	</div>
</article>