var contactForm = document.getElementById("game_submit");
if(contactForm){
	contactForm.addEventListener('submit', function(e){
		e.preventDefault();
		sendContactInfo(e);
	})
}
function sendContactInfo(event){
	var ajaxUrl = event.target.getAttribute('data-url');
	var form = event.target,
	name = document.getElementById('name'),
	email = document.getElementById('email'),
	game_link = document.getElementById('game_link'),
	game_description = document.getElementById('game_description'),
	game_type_battlefield = document.getElementById('battlefield'),
	game_type_open_world = document.getElementById('open_world'),
	game_type_other = document.getElementById('other'),
	btn = form.querySelector('[type="submit"]'),
	loader = document.getElementById('loader'),
	success_submition = document.querySelector('.game_success'),
	error_submition = document.querySelector('.game_error');

	formData = new FormData();
	formData.append('name', name.value);
	formData.append('email', email.value);
	formData.append('game_link', game_link.value);
	formData.append('game_type_battlefield', game_type_battlefield.checked ? 'battlefield' : null);
	formData.append('game_type_open_world', game_type_open_world.checked ? 'open world' : null);
	formData.append('game_type_other', game_type_other.checked ? 'other' : null);
	formData.append('game_description', game_description.value);
	formData.append('action', 'save_game_submission');

	if( name.value === '' ){
		name.classList.add('is-error');
		return;
	}else{
		name.classList.remove('is-error');
	
	}
	if( email.value === '' ){
		email.classList.add('is-error');
		return;
	}else{
		email.classList.remove('is-error');
	
	}
	if( game_link.value === '' ){
		game_link.classList.add('is-error');
		return;
	}else{
		game_link.classList.remove('is-error');
	
	}

	if( game_type_battlefield.checked  === false 
		&& game_type_open_world.checked  === false 
		&& game_type_other.checked === false){
		game_type_battlefield.parentElement.classList.add('is-error');
		game_type_open_world.parentElement.classList.add('is-error');
		game_type_other.parentElement.classList.add('is-error');
		return;
	}else{
		game_type_battlefield.parentElement.classList.remove('is-error');
		game_type_open_world.parentElement.classList.remove('is-error');
		game_type_other.parentElement.classList.remove('is-error');
	}

	if( game_description.value === '' ){
		game_description.classList.add('is-error');
		return;
	}else{
		game_description.classList.remove('is-error');
	}

	loader.classList.remove('hidden_form_inf');
	game_link.disabled = true;
	name.disabled = true;
	email.disabled = true;
	game_type_battlefield.disabled = true;
	game_type_open_world.disabled = true;
	game_type_other.disabled = true;
	game_description.disabled = true;
	btn.disabled = true;

	var options = {
		method: 'POST',
		body: formData
	};
	var req = new Request(ajaxUrl, options);
    
	fetch(req)
		.then((response)=>{
			if(response.ok){
				setTimeout(function(){
					form.reset();
					loader.classList.add('hidden_form_inf');
					success_submition.classList.remove('hidden_form_inf');
				}, 1000);
			}else{
				loader.classList.add('hidden_form_inf');
				error_submition.classList.remove('hidden_form_inf');
				throw new Error('Bad HTTP');	
			}
		})
		.catch((err) =>{
			if(err.message){
				loader.classList.add('hidden_form_inf');
				error_submition.classList.remove('hidden_form_inf');
			    console.log(err.message);
			}else{
				loader.classList.add('hidden_form_inf');
				error_submition.classList.remove('hidden_form_inf');
			    console.log('error');
			}
		});
}