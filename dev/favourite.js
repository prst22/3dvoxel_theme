const ajaxUrl = url.ajaxurl,
gamesCnt = document.querySelector('.fav_games'),
loader = document.querySelector('.games_cnt'),
slideUp = {
  ease: 'ease-in-out',
  origin: 'bottom',
  scale: 0.9,
  distance: '20%',
  opacity: 0,
  delay: 70,
  duration: 1000,
  interval: 100
};
let showFavouriteGames = (e) => {
	let formData = new FormData(),
	favGmesArr = localStorage.getItem('favouriteGames');

	formData.append('json', favGmesArr);
	formData.append('action', 'favourite_games');

	let options = {
	  	method: 'POST',
	  	body: formData
	},
	req = new Request(ajaxUrl, options);

	let favGamesPage = requestGames(e, req);

	favGamesPage.then(favGames => {
	  if(favGames !== '' && favGames !== undefined){
	    loader.parentNode.removeChild(loader);
	    gamesCnt.innerHTML = favGames; 
	  }else{
	    throw new Error('Something went wrong, pls refresh the page'); 
	  }
	})
	.then(page => {
		ScrollReveal().reveal('.index_game', slideUp);
	})
	.catch((err) =>{
	  alert(err.message);
	});
};

let requestGames = (e, req) => {

  let result = fetch(req)
  .then((response)=>{
    if(response.ok){
      return response.text();
    }else{
      throw new Error('Something went wrong, pls refresh the page'); 
    }
  })
  .catch((err) =>{
    if(err.message){
        console.log(err.message);
    }else{
        console.log('Something went wrong, pls refresh the page');
    }
  });
  
  return result;
};
window.addEventListener('load', showFavouriteGames());