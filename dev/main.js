/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/jump.js/dist/jump.module.js
// Robert Penner's easeInOutQuad

// find the rest of his easing functions here: http://robertpenner.com/easing/
// find them exported for ES6 consumption here: https://github.com/jaxgeller/ez.js

var easeInOutQuad = function easeInOutQuad(t, b, c, d) {
  t /= d / 2;
  if (t < 1) return c / 2 * t * t + b;
  t--;
  return -c / 2 * (t * (t - 2) - 1) + b;
};

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) {
  return typeof obj;
} : function (obj) {
  return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
};

var jumper = function jumper() {
  // private variable cache
  // no variables are created during a jump, preventing memory leaks

  var element = void 0; // element to scroll to                   (node)

  var start = void 0; // where scroll starts                    (px)
  var stop = void 0; // where scroll stops                     (px)

  var offset = void 0; // adjustment from the stop position      (px)
  var easing = void 0; // easing function                        (function)
  var a11y = void 0; // accessibility support flag             (boolean)

  var distance = void 0; // distance of scroll                     (px)
  var duration = void 0; // scroll duration                        (ms)

  var timeStart = void 0; // time scroll started                    (ms)
  var timeElapsed = void 0; // time spent scrolling thus far          (ms)

  var next = void 0; // next scroll position                   (px)

  var callback = void 0; // to call when done scrolling            (function)

  // scroll position helper

  function location() {
    return window.scrollY || window.pageYOffset;
  }

  // element offset helper

  function top(element) {
    return element.getBoundingClientRect().top + start;
  }

  // rAF loop helper

  function loop(timeCurrent) {
    // store time scroll started, if not started already
    if (!timeStart) {
      timeStart = timeCurrent;
    }

    // determine time spent scrolling so far
    timeElapsed = timeCurrent - timeStart;

    // calculate next scroll position
    next = easing(timeElapsed, start, distance, duration);

    // scroll to it
    window.scrollTo(0, next);

    // check progress
    timeElapsed < duration ? window.requestAnimationFrame(loop) // continue scroll loop
    : done(); // scrolling is done
  }

  // scroll finished helper

  function done() {
    // account for rAF time rounding inaccuracies
    window.scrollTo(0, start + distance);

    // if scrolling to an element, and accessibility is enabled
    if (element && a11y) {
      // add tabindex indicating programmatic focus
      element.setAttribute('tabindex', '-1');

      // focus the element
      element.focus();
    }

    // if it exists, fire the callback
    if (typeof callback === 'function') {
      callback();
    }

    // reset time for next jump
    timeStart = false;
  }

  // API

  function jump(target) {
    var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

    // resolve options, or use defaults
    duration = options.duration || 1000;
    offset = options.offset || 0;
    callback = options.callback; // "undefined" is a suitable default, and won't be called
    easing = options.easing || easeInOutQuad;
    a11y = options.a11y || false;

    // cache starting position
    start = location();

    // resolve target
    switch (typeof target === 'undefined' ? 'undefined' : _typeof(target)) {
      // scroll from current position
      case 'number':
        element = undefined; // no element to scroll to
        a11y = false; // make sure accessibility is off
        stop = start + target;
        break;

      // scroll to element (node)
      // bounding rect is relative to the viewport
      case 'object':
        element = target;
        stop = top(element);
        break;

      // scroll to element (selector)
      // bounding rect is relative to the viewport
      case 'string':
        element = document.querySelector(target);
        stop = top(element);
        break;
    }

    // resolve scroll distance, accounting for offset
    distance = stop - start + offset;

    // resolve duration
    switch (_typeof(options.duration)) {
      // number in ms
      case 'number':
        duration = options.duration;
        break;

      // function passed the distance of the scroll
      case 'function':
        duration = options.duration(distance);
        break;
    }

    // start the loop
    window.requestAnimationFrame(loop);
  }

  // expose only the jump method
  return jump;
};

// export singleton

var singleton = jumper();

/* harmony default export */ var jump_module = (singleton);

// closest() polyfill start
if (!Element.prototype.matches) {
  Element.prototype.matches = Element.prototype.msMatchesSelector || 
                              Element.prototype.webkitMatchesSelector;
}

if (!Element.prototype.closest) {
  Element.prototype.closest = function(s) {
    var el = this;

    do {
      if (el.matches(s)) return el;
      el = el.parentElement || el.parentNode;
    } while (el !== null && el.nodeType === 1);
    return null;
  };
}
// closest() polyfill end

// CONCATENATED MODULE: ./some_theme/dev/main-wp.js

let btn = document.getElementById('jump'),
btnGoToGames = document.getElementById('go_to_games'),
searchTrigger = document.querySelectorAll('.search_trigger>a'),
searchCnt =  document.getElementById('search_block'),
closeSearch = document.getElementById('close_search'),
menuBtn = document.getElementById('mobile_menu_btn'),
closeMenuBtn = document.getElementById('close_menu_btn'),
mobileMenu = document.querySelector('.mobile_menu_cnt'),
mobileMenuList = document.querySelector('.mobile_menu_cnt__inner');

btn.addEventListener('click', ()=>{
	jump_module('.site_header_cnt', {
	  duration: 1000,
	  offset: 0,
	});
});
if(btnGoToGames){
  btnGoToGames.addEventListener('click', (e)=>{
    e.preventDefault();
    if(document.querySelector('.games_to_play')){
      jump_module('.games_to_play', {
        duration: 1000,
        offset: 0,
      });
    }
  });
}

let slideUp = {
  ease: 'ease-in-out',
  origin: 'bottom',
  scale: 0.9,
  distance: '20%',
  opacity: 0,
  delay: 70,
  duration: 1000,
  interval: 100
},
slideLeft = {
  ease: 'ease-in-out',
  origin: 'left',
  distance: '10%',
  opacity: 0,
  delay: 70,
  duration: 1000,
  interval: 100,
  reset: false
},
slideRight = {
  ease: 'ease-in-out',
  origin: 'bottom',
  distance: '10%',
  opacity: 0,
  delay: 70,
  duration: 1000,
  interval: 100,
  reset: false
},
sword = {
  ease: 'ease',
  origin: 'right',
  distance: '40%',
  opacity: 0,
  delay: 400,
  duration: 900,
  viewFactor: 1.0,
  reset: false
};


ScrollReveal().reveal('.index_game', slideUp);
ScrollReveal().reveal('.showcase_left__item', slideLeft);
ScrollReveal().reveal('.showcase_right__item', slideRight);
ScrollReveal().reveal('.sword', sword);

// mobile menu start
function menuOn(){
  jQuery(document).ready(function($) {
    $(function() {  
        $(".mobile_menu_cnt__inner").niceScroll({
          cursorcolor: "#fd6877",
          cursorborder: "1px solid #fd6877",
          autohidemode: false,
          cursorwidth: "4px",
          cursorborderradius: 3,
          zindex: 800
        });
    });
  }); 
}
if(!window.matchMedia("(min-width: 901px)").matches){
  menuOn();
}

if(menuBtn){
  menuBtn.addEventListener('click', toggleMobileMenu);
  closeMenuBtn.addEventListener('click', toggleMobileMenu);
}

if(mobileMenu){
  function controllVpSize(x) {
    if (x.matches && document.body.classList.contains('mobile_menu_active')) { // If media query matches
      toggleMobileMenu(); 
    } 
  }

  function initializeMobileMenu(x) {
    if (!x.matches) { // If media query matches
      menuOn();
    } 
  }

  let x = window.matchMedia("(min-width: 901px)");
  controllVpSize(x); // Call listener function at run time
  x.addListener(controllVpSize);
  x.addListener(initializeMobileMenu);
  document.addEventListener('keydown', closeMenuOnEsc);
}

function toggleMobileMenu(e){
  function openCloseMenuWithNoTransitionDelay(){
    mobileMenu.classList.remove('transition_delay');
    document.body.classList.toggle('mobile_menu_active');
    mobileMenu.classList.toggle('hidden');
    mobileMenuList.classList.toggle('hidden');
  }
  if(e){ //check event to target click 
    if(e.target.closest('button') === closeMenuBtn 
      || e.key == "Escape" 
      || e.key == "Esc" 
      || e.keyCode == 27){ //choose open or close or esc button to determin whether to use transition delay
      document.body.classList.toggle('mobile_menu_active');
      mobileMenu.classList.add('transition_delay');
      mobileMenu.classList.toggle('hidden');
      mobileMenuList.classList.toggle('hidden');
    }else{
      openCloseMenuWithNoTransitionDelay();
    }
  }else{
    openCloseMenuWithNoTransitionDelay();
  }
  mobileMenu.addEventListener(transitionEvent, ()=>{
    jQuery(document).ready(function($) {
        $('.mobile_menu_cnt__inner').getNiceScroll().resize();
    });
  });

 
}
 
function closeMenuOnEsc(evt){
    evt = evt || window.event;
    let isEscape = false;
    if("key" in evt){
        isEscape = (evt.key == "Escape" || evt.key == "Esc");
    }else{
        isEscape = (evt.keyCode == 27);
    }
    if(isEscape && document.body.classList.contains('mobile_menu_active') 
      && !document.body.classList.contains('search_active')){
      toggleMobileMenu(evt);
    }

    if(isEscape && document.body.classList.contains('mobile_menu_active') 
      && document.body.classList.contains('search_active')){
      closeSearchModal();
    }
}
function whichTransitionEvent(){
  let t,
      el = document.createElement("fakeelement");

  let transitions = {
    "transition"      : "transitionend",
    "OTransition"     : "oTransitionEnd",
    "MozTransition"   : "transitionend",
    "WebkitTransition": "webkitTransitionEnd"
  }

  for (t in transitions){
    if (el.style[t] !== undefined){
      return transitions[t];
    }
  }
}

let transitionEvent = whichTransitionEvent();

// mobile menu end

// sorting by popularity start

if(document.body.classList.contains('home')){

  history.replaceState({page: location.href}, null, location.href);

  const ajaxUrl = document.querySelector('.sorting_type').getAttribute('data-url'),
  gamesContainer = document.getElementsByClassName('games_cnt')[0],
  sortBtn = document.getElementById('sort');
  let paginationCnt = document.getElementsByClassName('pagination_links')[0];

  let updatePagination = function (){
    let pagLinks = document.querySelectorAll('.pagination_links>a.page-numbers');
    for(let i = 0; i < pagLinks.length; i++){
      pagLinks[i].addEventListener('click', goToNewPage);
    }
  };
  
  let whichEvent = (event) => {
    let whatEvent = event.type,
    targetPaginationPage;
    if(whatEvent == 'click'){
      event.preventDefault();
      return targetPaginationPage = (!isNaN(Number(event.target.textContent))) ? Number(event.target.textContent) : 1;
    } else if(whatEvent == 'popstate') {
      let pat = /\/page\/(\d+)/;
      let num = event.state.page.match(pat);
      return targetPaginationPage = (num !== null) ? num[1] : 1;
    }
  };

  let loaderHideShow = (e) => {
    let sortLoader = document.querySelector('.sorting_type .loading'),
    gamesLoader = document.querySelector('.loader_overlay');
    sortBtn.disabled = !sortBtn.disabled;
    if(sortLoader) sortLoader.classList.toggle('hidden_form_inf');
    if(gamesLoader) gamesLoader.classList.toggle('hidden_loader');
  };
  let goToNewPage = (e) => {
    loaderHideShow(e);
    let formData = new FormData(),
    sortingKey = document.querySelector('.sorting_type').querySelector("input[name='sorting-type']:checked").getAttribute('data-type'),
    targetPage = whichEvent(e);

    formData.append('page_number', targetPage);
    formData.append('meta_key', sortingKey);
    formData.append('action', 'sort_games');

    let options = {
      method: 'POST',
      body: formData
    },
    req = new Request(ajaxUrl, options);

    let newGamePage = makeSortingRequest(e, req);

    newGamePage.then(newGameposts => {
      if(newGameposts !== '' && newGameposts !== undefined){
        gamesContainer.innerHTML = '';
        gamesContainer.innerHTML = newGameposts;
      }else{
        throw new Error('Something went wrong, pls refresh the page'); 
      }
    })
    .then(page => {
      if(e.type !== 'popstate') updateUrl(document.getElementsByClassName('pagination_links')[0]);
      paginationCnt = document.getElementsByClassName('pagination_links')[0];
      if(paginationCnt) updatePagination();

      if(document.querySelector('.games_to_play')){
        jump_module('.games_to_play', {
          duration: 1000,
          offset: 0,
          callback: () => { 
            setTimeout(() => ScrollReveal().sync(), 0)
            loaderHideShow(e);
          }
        });
      }
      
    })
    .catch((err) =>{
      alert(err.message);
      loaderHideShow(e);
    });
  };

  let makeSortingRequest = (e, req) => {

      let result = fetch(req)
      .then((response)=>{
        if(response.ok){ 
          return response.text();
        }else{
          throw new Error('Something went wrong, pls refresh the page'); 
        }
      })
      .catch((err) =>{
        if(err.message){
            console.log(err.message);
        }else{
            console.log('Something went wrong, pls refresh the page');
        }
      });
      
      return result;
  };

  function updateUrl(element){
    let previousPageData = element.getAttribute('data-page');
    history.pushState({ page: element.getAttribute('data-page') }, null, element.getAttribute('data-page'));
    return false;
  }
  
  window.addEventListener('popstate', (event) => {
    if(event.state === null){
      event.preventDefault();
      return false;
    }
    let character = event;
    if(character) goToNewPage(character);
  });

  updatePagination();
  sortBtn.addEventListener('click', goToNewPage);
}

// sorting by popularity end

// search modal start

if(searchTrigger.length != 0){
  for (let i = 0; i < searchTrigger.length; i++) {
    searchTrigger[i].addEventListener('click', (e) =>{
      e.preventDefault();
      closeSearchModal();
      document.addEventListener('keydown', closeSearchOnEsc);
    });
  }

  closeSearch.addEventListener('click', (e) =>{
    closeSearchModal();
  });
}
function closeSearchModal(){
  document.body.classList.toggle('search_active', !document.body.classList.contains('search_active'));
  searchCnt.classList.toggle('hidden_search', !searchCnt.classList.contains('hidden_search'));
}
function closeSearchOnEsc (evt){
    evt = evt || window.event;
    let isEscape = false;
    if("key" in evt){
        isEscape = (evt.key == "Escape" || evt.key == "Esc");
    }else{
        isEscape = (evt.keyCode == 27);
    }
    if(isEscape){
      document.body.classList.remove('search_active');
      searchCnt.classList.add('hidden_search');
      document.removeEventListener('keydown', closeSearchOnEsc);
    } 
}

// search modal end



if(document.body.classList.contains('games-template-default')){
  //add to favourite start
  let addToFavBtn = document.getElementById('add_to_favourite'),
  removeGameBtn = document.getElementById('remove_favourite'),
  game = postID.id,
  localStorGames = JSON.parse(localStorage.getItem('favouriteGames')),
  isGameAlreadyInLocSt = localStorGames != null ? localStorGames.find((gameID)=>{ return parseInt(gameID) == (game) }) : null;

  removeGameBtn.disabled = true;

  window.addEventListener('DOMContentLoaded', ()=>{

    addToFavBtn.addEventListener('click', addGameToFav);

    removeGameBtn.addEventListener('click', ()=>{
      removeGameFromFav(game);
    });

  });

  function addGameToFav(){
    if(!localStorGames){
      localStorGames = [];
      localStorGames.push(game);
      localStorage.setItem('favouriteGames', JSON.stringify(localStorGames));
      isGameAlreadyInLocSt = localStorGames != null ? localStorGames.find((gameID)=>{ return parseInt(gameID) == (game) }) : null;
      addGameBtnState(isGameAlreadyInLocSt);
    }else{
      if(isGameAlreadyInLocSt == undefined){
        if(localStorGames.length <= 35){
          addNewGameItem();
        }else{
          localStorGames.splice(0, 1);
          addNewGameItem();
        }
      }
    }
  }

  function addNewGameItem(){
    localStorGames.push(game);
    localStorage.setItem('favouriteGames', JSON.stringify(localStorGames));
    isGameAlreadyInLocSt = localStorGames.find((gameID)=>{ return parseInt(gameID) == (game) });
    addGameBtnState(isGameAlreadyInLocSt);
  }

  function removeGameFromFav(gameToRemove){
    let index = localStorGames.indexOf(gameToRemove);
    if(index > -1){
      localStorGames.splice(index, 1);
      localStorage.setItem('favouriteGames', JSON.stringify(localStorGames));
      isGameAlreadyInLocSt = localStorGames != null ? localStorGames.find((gameID)=>{ return parseInt(gameID) == (game) }) : null;
      addToFavBtn.disabled = !addToFavBtn.disabled;
      removeGameBtn.disabled = !removeGameBtn.disabled;
      addToFavBtn.textContent = addToFavBtn.disabled == true ? 'game in favourite list' : 'add to favourite';
    }
  }

  function addGameBtnState(ifGameAlreadyInStor){
    if(localStorGames && ifGameAlreadyInStor != undefined && ifGameAlreadyInStor != null){
      addToFavBtn.textContent = addToFavBtn.disabled == false ? 'game in favourite list' : 'add to favourite';
      addToFavBtn.disabled = !addToFavBtn.disabled;
      removeGameBtn.disabled = !removeGameBtn.disabled;
    }
  }

  addGameBtnState(isGameAlreadyInLocSt);
  //add to favourite end

  // adblock detection notice start

  if( window.canRunAds === undefined ){
  // adblocker detected, show fallback
    let gameContainer = document.getElementsByClassName('single_post')[0],
    toastOuterCnt = document.createElement('div'),
    toastCnt = document.createElement('div'),
    removeToastBtn = document.createElement('button'),
    toastTextCnt = document.createElement('p'),
    toastHeading = document.createElement('h4'),
    toastHeadingText = document.createTextNode("AdBlock detected"),
    toastReloadLink = document.createElement('a'),
    toastText = `Please note that it is necessary to disable adblock to play some of our games.
    Disable adblock and <a href="${window.location.href}" class="btn btn-primary btn-sm yellow_btn">reload page</a>`;
    toastOuterCnt.className = 'adblock_notice';
    toastCnt.className = 'toast';
    removeToastBtn.className = 'btn btn-clear float-right close_notice';

    toastTextCnt.innerHTML = toastText;
    toastHeading.appendChild(toastHeadingText);

    toastOuterCnt.appendChild(toastCnt);
    toastCnt.appendChild(toastHeading);
    toastCnt.appendChild(removeToastBtn);
    toastCnt.appendChild(removeToastBtn);
    toastCnt.appendChild(toastTextCnt);

    document.getElementsByClassName('single_post_main__inner')[0].insertBefore(toastOuterCnt, gameContainer);

    if(removeToastBtn){
      removeToastBtn.addEventListener('click', ()=>{
        toastOuterCnt.style.maxHeight = '0px';
      });
      toastOuterCnt.addEventListener(transitionEvent, (e)=>{
        if(e.target.classList.contains('adblock_notice')){
          toastOuterCnt.parentNode.removeChild(toastOuterCnt);
        }
      }, false);
    }
    

  }
  // adblock detection notice end
}



/***/ })
/******/ ]);


