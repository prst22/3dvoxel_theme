<?php get_header(); ?>

	<main class="container grid-xl main archive_page">
        <div class="columns main__inner">

		<?php
			if ( have_posts() ) {

				echo '<div class="column col-12">';
				echo '<header >';
				
					the_archive_title( '<h1 class="archive_page__title">', '</h1>' );
			
			    echo '</header>';
			    if(category_description()){
			    	echo category_description();
			    }
			    echo '<div class="separator"></div>';
			    echo '</div>';

				while ( have_posts() ) : the_post();

					get_template_part( 'template-parts/one', 'post' );

				endwhile;

				if($wp_query->max_num_pages > 0): ?>	
					
					<div class="column col-12">
						<div class="pagination_cnt pagination_links pagination_links--numbered">
							<?php echo paginate_links(
									array(
										'prev_next'          => false,
										'type'               => 'plain',
										'end_size'           => 2,
										'mid_size'           => 2,
									)
								); ?>
						</div>
					</div>
					
				<?php endif; ?>

			<?php } else { ?>

				<h1>Nothing to show(</h1>

			<?php }
		?>

		</div>
	</main>

<?php
get_footer();