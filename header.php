<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="description" content="<?php bloginfo('description'); ?>">
	<title>
		<?php bloginfo('name');?> |
		<?php is_front_page() ? bloginfo('description') : wp_title('');?> 
	</title>
	<meta name="keywords" content="Games, minecraft, minecraft games, 3d games, voxel games">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_stylesheet_directory_uri(); ?>/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon-16x16.png">
	<link rel="manifest" href="<?php echo get_stylesheet_directory_uri(); ?>/site.webmanifest">
	<link rel="mask-icon" href="<?php echo get_stylesheet_directory_uri(); ?>/safari-pinned-tab.svg" color="#5bbad5">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="theme-color" content="#ffffff">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<?php wp_head(); ?>
	<style>
		/*reveal start*/
		html.sr .index_game, html.sr .showcase_right__item, html.sr .showcase_left__item, html.sr .sword{
		  visibility: hidden;
		}
		/*reveal end*/
	</style>
</head>
<body <?php body_class(); ?>>
<section class="search_block hidden_search" id="search_block">
	<div class="search_block__close_search_cnt" id="close_search">
		<button class="btn btn-primary btn-action">
			<span class="icon icon-close"></span>
		</button>
	</div>
	<div class="search_block__inner">
		<img class="search_block_img" src="<?php echo esc_url(get_template_directory_uri() . '/assets/face.png'); ?>" alt="face">
    	<?php get_search_form(); ?>
	</div>	
</section>	
<div class="overflow-search"><!--menu wrapper for blur effect search -->

<div class="overflow"><!--menu wrapper for blur effect -->
	<div class="site_wrap">

		<section class="container site_header_cnt">
			<div class="columns">
				<div class="column col-sm-9 col-md-7 col-lg-3 col-xl-4 col-4">
					<header class="site_header_cnt__site_header">
						<a href="<?php echo home_url();?>" title="Go home" class="main_site_logo">
							<img src="<?php echo esc_url(get_template_directory_uri() . '/assets/logo.png'); ?>" alt="<?php bloginfo('name'); ?>">
						</a>
						<a href="<?php echo home_url();?>" title="Go home" class="main_site_header">
							<?php echo bloginfo('name'); ?>
						</a>
					</header>
				</div>
				<?php if ( has_nav_menu( 'main_menu' ) ) : ?>
					<div class="column col-sm-3 col-md-5 col-lg-9 col-xl-8 col-8">
						<nav class="nav">
							<div class="mobile_menu_toggle show-md hide-lg hide-xl">
							    <div class="mobile_menu_toggle__button">
							    	<button class="btn btn-primary btn-lg btn-action" id="mobile_menu_btn">
										<span class="icon icon-th-menu"></span>
									</button>
							    </div>
							</div>
							<?php
							   wp_nav_menu( array(
								   'menu'              => 'Main menu',
								   'theme_location'    => 'main_menu',
								   'menu_class'        => 'top_menu',
								   'depth'             => 1,          
								   'container_class'   => 'nav__inner hide-md',           
								   )
							   );
							?>

						</nav>

					</div>
				<?php endif; ?>
				
			</div>
		</section>