<form method="get" class="searchform form-inline search_form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<div class="input-group">
		<span class="input-group-addon"><span class="icon icon-gamepad"></span></span>
	  	<input type="text" class="form-input input-lg search_field" name="s" placeholder="<?php esc_attr_e( 'Game search' ); ?>" title="search" size="22">
	  	<button class="btn btn-primary btn-lg input-group-btn" type="submit" name="submit"><span class="icon icon-search"></span></button>
	</div>
</form>