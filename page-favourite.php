<?php
/* Template Name: Favourite game */
?>
<?php get_header();?>
    <main class="container grid-xl main">
        <div class="columns single_page main__inner">
            <div class="column col-12">
                <header class="single_page_heading">
                    <h1 class="heading_title"><?php the_title(); ?></h1>
                </header>
            </div>
            <div class="column col-12 single_page__favourite games_cnt">
                <div class="loader_overlay">
                    <i class="form-icon loading"></i>
                </div>
            </div>
        </div>
        <div class="columns fav_games"></div>
        
    </main>
<?php get_footer();?>