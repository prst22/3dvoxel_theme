<?php
	get_header();
?>
<main class="container grid-xl main">

	<div class="columns main__inner">
		    
		<?php

			if (have_posts()) : while ( have_posts() ) : the_post();
				
				get_template_part( 'template-parts/one','post' ); 
									
				endwhile;

				else: 
                    get_template_part( 'template-parts/one','nopost' );

			 endif; ?>	

			<?php 
				if($wp_query->max_num_pages > 0): ?>
					
					<div class="column col-12">
						<div class="pagination_cnt pagination_links">
							<?php previous_posts_link( '<span class="icon icon-arrow-left-thick px-1"></span>prev page '); ?>
							<?php next_posts_link( 'next page <span class="icon icon-arrow-right-thick px-1"></span>'); ?>
						</div>
					</div>
					
			<?php endif; ?>
	</div>
</main>

<?php		
	get_footer();
?>