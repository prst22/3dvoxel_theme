<?php
	get_header();
?>
<?php $total_results = $wp_query->found_posts; ?>
<main class="container grid-xl main">

	<div class="columns main__inner">

		<header class="column col-12 search_header_cnt">
            <h1 class="search_header_cnt__item">
				<?php printf( __( 'Search Results for: <span class="search_query">%s</span>', '3dvoxel-theme' ), get_search_query() ); ?>
				<?php echo "<span class=\"search_query_count\"> - $total_results</span>" ?>
			</h1>
			<div class="search_header_cnt__item search_results_page_form">
				<?php get_search_form(); ?>
			</div>
		</header>

		    
		<?php

			if (have_posts()) : while ( have_posts() ) : the_post();
				
				get_template_part( 'template-parts/one','post' ); 
									
				endwhile;

				else:?> 
                    
					<div class="column col-12 nothing_found">
						<div class="search_face">
							<img src="<?php echo esc_url(get_template_directory_uri() . '/assets/sad.svg'); ?>" alt="dull face" >
						</div>
						<h2>
							Nothing found
						</h2>
					</div>

			<?php endif; ?>	

			<?php 
				if($wp_query->max_num_pages > 0): ?>
					
					<div class="column col-12">
						<div class="pagination_cnt pagination_links pagination_links--numbered">
							<?php echo paginate_links(
									array(
										'prev_next'          => false,
										'type'               => 'plain',
										'end_size'           => 2,
										'mid_size'           => 2,
									)
								); ?>
						</div>
					</div>
					
			<?php endif; ?>
	</div>
</main>

<?php		
	get_footer();
?>