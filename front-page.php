<?php
	get_header();
?>
<?php 

if(is_front_page()):?>
<section class="showcase_img_cnt">
	<div class="container grid-xl h">
  		<div class="columns h">
    		<div class="column col-md-12 col-6 showcase_left">
    			<div class="showcase_left__item">
    				<h1>
    					<span class="primary">Sci-Fi Shooter collect the best online 3D voxel games.</span> Here you go, my collection of various fun games for your enjoyment!
	    			</h1>
    				<a href="#" class="btn btn-primary" id="go_to_games">GAMES TO PLAY<span class="icon icon-arrow-down-thick"></span></a>
    			</div>
    		</div>
    		<div class="column col-md-12 col-6 showcase_right">
    			<div class="showcase_right__item">
    				<h3>
	    				Want to show your game? 
	    			</h3>
    				<a href="<?php echo esc_attr(get_permalink( get_page_by_title( 'Want to show your game?' ) )); ?>" class="btn btn-primary btn-sm">Click here!</a>
    			</div>
    			<div class="sword"></div>
    		</div>
    	</div>
    </div>
</section>
<?php endif; ?>

<main class="container grid-xl main">
	<div class="main__inner">
		<div class="columns sorting_cnt">
			<div class="column col-12">
				<h2>SORT GAMES BY:</h2> 
			</div> 
			<?php
			
	            $categories = get_categories(array(
	            	'taxonomy' => 'game_types',
				    'orderby'  => 'name',
				    'order'    => 'ASC'
				));

				$count_game_posts = wp_count_posts('games')->publish;

	            if($count_game_posts > 0): ?>

					<div class="column col-lg-12 col-xl-7 col-6">
		            	
	            		<h3>GAME TYPE</h3>
	            		<div class="game_type_sub_head">Click the category buttons to sort the games!</div>

		            	<div class="game_t_block">
		            	 	<div class="game_types_cnt"> 
		            	 		<div class="game_types_cnt__inner">
									<?php

									foreach( $categories as $category ) {
										
										    $category_link = sprintf( 
										        '<a href="%1$s" class="btn btn-primary badge btn_game_type" title="%2$s" data-badge="%3$s">%4$s games</a>',
										        esc_url( get_category_link( $category->term_id ) ),
										        esc_attr( sprintf( __( 'View all games in %s', '3dvoxel-theme' ), $category->name ) ),
										        esc_html__($category->count),
										        esc_html( $category->name )
										    );

											echo $category_link;  
									} ?>

								</div> 
							</div> 
						</div>
					</div>

				<?php endif; ?>
				<div class="column col-lg-12 col-xl-5 col-6">
					<h3>NEW/POPULAR</h3>
					<div class="game_type_sub_head">Choose sorting type</div>
					<div class="game_t_block">
	            	 	<div class="game_types_cnt"> 
	            	 		<div class="game_types_cnt__inner sorting_type" data-url="<?php echo admin_url('admin-ajax.php'); ?>">
	            	 			<div class="form-group">
								  <label class="form-radio form-inline">
								    <input type="radio" name="sorting-type" <?php echo isset($_SESSION['sort_type']) ? ( ($_SESSION['sort_type'] == 'new') ? 'checked' : '' ) : 'checked'; ?> data-type="sort_new"><i class="form-icon"></i> New
								  </label>
								  <label class="form-radio form-inline">
								    <input type="radio" name="sorting-type" <?php echo (isset($_SESSION['sort_type']) && $_SESSION['sort_type'] == 'popularity') ? 'checked' : ''; ?> data-type="sort_popularity"><i class="form-icon"></i> Popular
								  </label>
								  <span class="sorting_btn_cnt">
								  	<button class="btn btn-primary" id="sort">SORT</button>
								  	<i class="form-icon loading hidden_form_inf"></i>
								  </span>
								  
								</div>

	            	 		</div>
	            	 	</div>
	            	 </div>
				</div>
		</div>
		<div class="columns">
			<?php 
				if($count_game_posts > 0){
					echo '<div class="column col-12 games_to_play"><h2>GAMES TO PLAY:</h2></div>';
				} 
			?>
		</div>
		<div class="columns games_cnt">
		<?php
			if(isset($_SESSION['sort_type'])){
				$order_by = $_SESSION['sort_type'] == 'new' ? 'date' : 'meta_value_num date';
				$meta_key = $_SESSION['sort_type'] == 'new' ? '' : 'voxel_theme_game_views';
			}else{
				$order_by = 'date';
				$meta_key = '';
			}
			$args = array(
		        'post_type'		=> 'games',
		        'post_status'	=> 'publish',
		        'orderby' 		=> $order_by,
				'meta_key'		=> $meta_key,
		        'paged' 		=> if_paged(1),
		        'order'     	=> 'DESC'
		    );

			$games_query = new WP_Query($args);

			if ( $games_query->have_posts() ) {
				echo '<div class="loader_overlay hidden_loader"><i class="form-icon loading"></i></div>';
				while ( $games_query->have_posts() ) {
					$games_query->the_post();			        
					get_template_part( 'template-parts/one', 'post' ); 
				} // end while
				wp_reset_postdata();
			}else{
				echo '<div class="loader_overlay hidden_loader"><i class="form-icon loading"></i></div>';
				get_template_part( 'template-parts/one', 'nopost' );
			}
			?>	
			
			<?php if($games_query->max_num_pages > 0): ?>	
			
				<div class="column col-12">
					<div class="pagination_cnt pagination_links pagination_links--numbered" data-number="<?php echo if_paged(1); ?>" data-url="<?php echo admin_url('admin-ajax.php'); ?>">
						
						<?php echo paginate_links(
							array(
								'base'               => '#',
								'format'             => '',
								'prev_next'          => false,
								'type'               => 'plain',
								'total'              => $games_query->max_num_pages,
								'current'            => if_paged(1),
								'end_size'           => 2,
								'mid_size'           => 2,
								)
							); ?>
					</div>
				</div>

			<?php endif; ?>
		</div>		
	</div>
</main>

<?php		
	get_footer();
?>