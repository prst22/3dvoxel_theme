<?php
/* Template Name: Suggest game */
?>
<?php get_header();?>
    <main class="container grid-xl main">
        <div class="columns">
            <div class="column col-12">
                <div class="single_page main__inner">
                    <?php while ( have_posts() ) : the_post(); ?>
                    <div class="single_page__inner">
                        <header class="single_page_heading">
                            <h1 class="heading_title"><?php the_title(); ?></h1>
                        </header>
                        <div class="content_here">
                            <?php
								require_once get_stylesheet_directory() . '/template-parts/suggest-game-form.php';
							?>
                        </div>
                    </div>
                    <?php endwhile; ?>
                </div>
            </div>
        </div>
    </main>
<?php get_footer();?>