<?php
	get_header();
?>
<main class="container grid-xl main">

	<div class="columns main__inner">

		<div class="column col-12 page_404">
			<div class="page_404__image_cnt">
				<img src="<?php echo esc_url(get_template_directory_uri() . '/assets/sad.svg'); ?>" alt="404" >
			</div>
    		<h2 class="text-center">Something went wrong 404 <br>Page not found</h2>
    	</div>
	</div>
</main>
<?php		
	get_footer();
?>