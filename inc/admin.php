<?php 
// login logo on wp-admin page
	function voxel_login_logo() { ?>
	    <style type="text/css">
	        #login h1 a, .login h1 a {
	            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/logo.svg);
	        }
	    </style>
	<?php }
	add_action( 'login_head', 'voxel_login_logo' );
//logo link on wp admin page
	function voxel_login_logo_url() {
	    return home_url();
	}
	add_filter( 'login_headerurl', 'voxel_login_logo_url' );