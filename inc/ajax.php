<?php 
	

	add_action('wp_ajax_nopriv_save_game_submission', 'save_game_submission');
	add_action('wp_ajax_save_game_submission', 'save_game_submission');


	function save_game_submission(){
	    $title = sanitize_text_field($_POST['name']);
	    $email = sanitize_email($_POST['email']);
	    $link = wp_strip_all_tags($_POST['game_link']);
	    $description = sanitize_text_field($_POST['game_description']);
	    $game_type_battlefield = wp_strip_all_tags($_POST['game_type_battlefield']);
	    $game_type_open_world = wp_strip_all_tags($_POST['game_type_open_world']);
	    $game_type_other = wp_strip_all_tags($_POST['game_type_other']);
	    $types = [];
	    if($game_type_battlefield == 'battlefield'){
	    	$types[] = $game_type_battlefield;
	    }
	    if($game_type_open_world == 'open world'){
	    	$types[] = $game_type_open_world;
	    }
	     if($game_type_other == 'other'){
	    	$types[] = $game_type_other;
	    }
	    $args = array(
	        'post_title' => $title,
	        'post_content' => $description .' from '.  $email,
	        'post_author' => 1,
	        'post_status' => 'draft',
	        'post_type' => 'games',
	        'meta_input' => array(
	            '_voxel_games_iframe_link_value_key'	=> $link,
	            'voxel_theme_game_views'				=> 0
	        ),
	       
	    );
	    $postID = wp_insert_post($args);  

	    if($postID !== 0){
	    	// setting custom tax (game type) start
	    	wp_set_object_terms($postID, $types, 'game_types');
	    	// setting custom tax (game type) end
	    	$to = get_bloginfo('admin_email');
	    	$subject  = 'New game was submitted - '. $title;

	    	$headers[] = 'From: ' . get_bloginfo('name') . '<'. $to .'>';
	    	$headers[] = 'Reply-To: '. $title . '<'. $email .'>';
	    	$headers[] = 'Content-Type: text/html; chartset=UTF-8';

	    	wp_mail($to, $subject, $description, $headers);
	    	echo $postID;
	    }else{
	    	echo 0;
	    }
	    
	    wp_die();
	}

	add_action('wp_ajax_nopriv_sort_games', 'sort_games');
	add_action('wp_ajax_sort_games', 'sort_games');

	function sort_games(){

		if(wp_doing_ajax()){
			$page_number = wp_strip_all_tags( $_POST["page_number"] );
			$page = ( $page_number == (int)$page_number ) ? (int)$page_number : 1;
			$meta_key_popularity = wp_strip_all_tags( $_POST["meta_key"] ) == 'sort_popularity' ?: '';
			$meta_key = !empty($meta_key_popularity) ? 'voxel_theme_game_views' : '';
			$order_by = !empty($meta_key_popularity) ? 'meta_value_num date' : 'date';
			$page_trail = get_site_url(null , null , 'relative') .'/';
			if($meta_key_popularity == 'sort_popularity'){
				$_SESSION['sort_type'] = 'popularity';
			}else{
				$_SESSION['sort_type'] = 'new';
			}
			
			$args = array(
		        'post_type'		=> 'games',
		        'orderby' 		=> $order_by,
				'meta_key'		=> $meta_key,
		        'post_status'	=> 'publish',
		        'paged' 		=> $page,
		        'order'     	=> 'DESC'
		    );
			
			$sorted_games_query = new WP_Query($args);

			if ( $sorted_games_query->have_posts() ) {
				echo '<div class="loader_overlay"><i class="form-icon loading"></i></div>';
				while ( $sorted_games_query->have_posts() ) {
					$sorted_games_query->the_post();			        
					get_template_part( 'template-parts/one', 'post' ); 
				} // end while
				
			}else{
				echo '<div class="loader_overlay"><i class="form-icon loading"></i></div>';
				get_template_part( 'template-parts/one', 'nopost' );
			}

			wp_reset_postdata();

			if($sorted_games_query->max_num_pages > 0): 

				echo '<div class="column col-12">
					<div class="pagination_cnt pagination_links pagination_links--numbered" data-page="' . $page_trail . 'page/'. $page .'">';
				
						echo paginate_links(
							array(
								'base'               => '#',
								'format'             => '',
								'prev_next'          => false,
								'type'               => 'plain',
								'total'              => $sorted_games_query->max_num_pages,
								'current'            => max( 1, $page ),
								'end_size'           => 2,
								'mid_size'           => 2,
							)
						);

					echo '</div>
				</div>';
					
			endif;
		}

		wp_die();
	}

	function if_paged($num = null){
		$output = '';
		if(is_paged()){ $output = 'page/'. get_query_var('page');}

		if($num == 1){
			$num_p = ( get_query_var('page') == 0 ? 1 : get_query_var('page') );
            return $num_p;
		}else{
			return $output;
		}
	}

	add_action('wp_ajax_nopriv_favourite_games', 'favourite_games');
	add_action('wp_ajax_favourite_games', 'favourite_games');

	function favourite_games(){
		if(wp_doing_ajax()){
			$fav_game_data_str = wp_strip_all_tags($_POST["json"]);
			$fav_game_data_str = trim($fav_game_data_str, "[]");
			$fav_game_data_str = str_replace('\"','', $fav_game_data_str);
			$fav_games_arr = !empty($fav_game_data_str) ? explode(',', $fav_game_data_str) : 0;
			$games = array();

			if($fav_games_arr != 0){
				foreach ($fav_games_arr as $gameID) {
					$gameID = trim($gameID);
					$games[] = (int)$gameID;
				}
			}else{
				$games[] = 0;
			}

			$args = array(
		        'post_type'		=> 'games',
		        'post_status'	=> 'publish',
		        'post__in' 		=> $games,
		        'posts_per_page'=> 36,
		        'orderby'        => 'title',
    			'order'          => 'ASC'
		    );
			
			$fav_games_loop = new WP_Query($args);

			if ( $fav_games_loop->have_posts() ) {
				while ( $fav_games_loop->have_posts() ) {
					$fav_games_loop->the_post();			        
					get_template_part( 'template-parts/one', 'favorite-game' ); 
				} // end while
				echo '<div class="column col-12 text-center">
			                <small><i>36 games max may be added to favourite list</i></small>
			            </div>';
			}else{
				echo '<div class="column col-12">
					<h3>NO GAMES IN YOUR FAVOURITE LIST SO FAR</h3>
					<p>To add a game, simply click "Add to favorite" in the game page.</p>
				</div>';
			}

			wp_reset_postdata();
		}
		wp_die();
	}